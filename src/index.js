import axios from './axios'
import exp from './exp';
import * as m from './md5'
import tools from './tools';
import directives from './directives';
export default {
  install(app, options) {
    app.config.globalProperties.$axios = axios;
    Object.keys(directives).forEach((key) => {
      app.directive(key, directives[key])
    })
  }
}

export const useAxios = axios;
export const useExp = exp;
export const md5 = m;
export const useTools = tools;