const path = require("path");

module.exports = {
    entry: './src/index.js',
    experiments: {
        outputModule: true,
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].js",
        library: {
            type: 'module',
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                },
                exclude: /node_modules/, //不转译node_modules目录的源代码。
            }
        ]
    },
    mode: 'production'
}