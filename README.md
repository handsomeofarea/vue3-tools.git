# 为vue3准备的常用函数工具包

## 1.安装

`npm i vue3-tools-cky`

## 2.全局使用axios

``` javascript 
 import vue3Tools from 'vue3-tools-cky'
 
 const app = createApp(App)
 app.use(vue3Tools);

```

### (1)组件内使用(不推荐setup使用)
 
 ```javascript 
 created() {
    const useAxios = this.$axios();
    const {getData} = useAxios(); //get请求
  }

 ```
 #### 1.1 axios 使用 ,推荐创建一个api文件，来使用useAxios,useAxios会创建一个axios实列，不提倡反复use

``` javascript 
 index.js
 import {useAxios} from 'vue3-tools-cky'
 const axios = useAxios();
 axios.sesetBaseUrl(url:String) //添加默认请求地址
 axios.setTimeout(time: Number) //超时时间
 axios.addHeader(key: String,value: String) //添加请求头
 axios.removeHeader(key: String);//删除请求头
 axios.setHeadersExcept(url: [])  //请求头排除的请求
 axios.changeIsWithCredentials(value: Boolean)//请求是否携带cookie凭证，默认true
 axios.setResultCodeHandler(value:Function) //设置处理响应code函数
 export defalut axios
```
#### 1.2 GET请求
```javascript 
   const {getData} = useAxios();
   const {data} = getData('xxxxx/page/vueRouter',params,options)

   getData传入3个参数：第一个参数url:String,第二个参数data:Object,第三个参数options: Object
```
#### 1.3 DELETE,HEAD 请求
``` javascript 
  const {deleteData} = useAxios();
  const {data} = deleteData('xxxxx/page/vueRouter',params,options)

  deleteData传入3个参数：第一个参数url:String,第二个参数data:Object,第三个参数options: Object
```

#### 1.4 postData 以Content-Type：application/x-www-form-urlencoded 请求
```javascript
   const { postData } = useAxios();
   const {data} = postData('xxxxx/page/vueRouter',params,options)
   postData传入3个参数：第一个参数url:String,第二个参数data:Object,第三个参数options: Object

   以x-www-form-urlencoded 还有putData，当然下面介绍以axios的默认格式Content-Type: application/json
```

#### 1.5 postJSON 以Content-Type: application/json  请求
```javascript
   const { postJSON } = useAxios();
   const {data} = postJSON('xxxxx/page/vueRouter',params,options)
   postJSON传入3个参数：第一个参数url:String,第二个参数data:Object,第三个参数options: Object

   当然还有putJSON,deleteJSON 用例同上
```

#### 1.6 getBlob 请求二进制文件
```javascript
   const { getBlob } = useAxios();
   const {data} = getBlob('xxxxx/page/vueRouter',params,options) 
```
#### 1.7 上传文件 请求头 以Content-Type:postMultipart

```javascript
   const { postMultipart } = useAxios();
   const {data} = postMultipart('xxxxx/page/vueRouter',params,options)
   postMultipart传入3个参数：第一个参数url:String,第二个参数data:Object,第三个参数options: Object
```
## 3.使用指令

``` javascript
    1. v-drag
    //拖拽
    <div v-drag="option"> </div>
    2.v-waterMark
    //水印
     (1):可添加参数option
         {
           text: '水印内容',
           angle: 25, //旋转度数
           color: 'rgba(0,0,0,.15)',
           fontSize: '16px',
         } 
        
```


## 4.正则校验

### 4.1 引入使用
```javascript
 import {useExp} from 'vue3-tools-cky'

 // 校验简单手机号
 const {isEasyMobilePhone} = useExp();
 //复杂手机号
 const {isMobilePhone} = useExp();
 //国内座机号
 const {telephone} = useExp();
 //身份证号
 const {idCard} = useExp();
 //IPV4
 const {isIpV4} = useExp();
 //十六进制
 const {isHex} = useExp();
 //QQ号码 
 const {isQQ} = useExp(); 
 //微信号码
 const {isWX} = useExp(); 
 //包含中文
 const {isIncludeCN} = useExp();
 //只有中文 
 const {isOnlyCN} = useExp(); 
 //URL
 const {isURL} = useExp();
 //HTTP 
 const {isHTTP} = useExp();
 //HTTPS 
 const {isHTTPs} = useExp(); 
 //邮箱验证
 const {isHTTPs} = useExp(); 
```

## 5.md5加密
```javascript
 import {md5} from 'vue3-tools-cky'
 md.md5B64()
 md.md5Hex()
 md.md5Str()
```


## 6.工具函数

```javascript
   import {useTools} from 'vue3-tools-cky'
   //数组去重
   const {uniq} = useTools();
   //数组最大值
   const {uniq} = useTools();
   // 数组最小值
   const {min} = useTools();
   /*** 对象属性排序
    * @param arr
    * @param attr 属性key
    * @param order 排序方式 asc 升序 desc 降序
    */
   const {sortBy} = useTools();
  
   /**
     * 构建树形结构(不会改变原数组)
     * @param {Array} arr 所有节点组成的数组
     * @param {String | null} rootId 根节点id的pid 如果没有传null
     * @param {String} idKey id的键名
     * @param {String} pidKey pid的键名
     * @returns {Array}
   */
   const {formatTree} = useTools();

   /**
     * JSON数组去重
     * @param {Array} arr JSON数组
     * @param {String} key 以某个key进行去重
     * @returns {Array} 返回新数组
     */
     const {removeJSONArrRepeat} = useTools();

     /**
       * 格式化时间戳
       * @param {Number | Date} time 毫秒时间戳 | 时间对象
       * @param {String} fmt 格式化方式
       * @returns {*}
      */
     const {formatDate} = useTools();

     /**
       * 随机数区间,取[min, max]
       * @param min 最小值
       * @param max 最大值
       * @returns {number}
      */
      const {random} = useTools();

      /**
        * 函数柯里化
        * @param fn {function} 需要函数柯里化的函数
        * @param args 需要被解耦的参数集
      */
      const {curring} = useTools();

      /**
        * 浏览器直接下载文件
        * @param blob {File | Blob} 文件blob | file 对象
        * @param fileName {String} 文件名
      */
      const {download} = useTools();
     /**
       * 加载图片完成
       * @param imgElement
       * @return {Promise<any>}
    */
    const {imgLoad} = useTools();

    /**
      * 压缩(放大)图片
      * @param imgFile {File} 图片文件对象
      * @param scale {Number} 压缩率
      * @param mime {String} 文件类型
      * @return {Promise<*|File>}
   */
    const {compressImage}  = useTools();

    /**
      * 压缩(放大)图片大小
      * @param imgFile {File} 图片文件对象
      * @param maxSize {Number} 文件大小基准点
      * @param isFixSize {Boolean} 是否开启文件大小修正 开启后会等到精度更高的压缩效果
      * @return {Promise<*|File>}
   */
   const {compressImageSize} = useTools();

    /**
      * 将base64转成file对象
      * @param base64 {String} 带mime前缀的base64字符串
      * @param fileName {String} 文件名(后缀强制为mime类型)
      * @return {File}
   */
   
   const {convertBase64ToFile} = useTools();
   /**
     * 文件对象转base64字符串
     * @param {File} file 文件对象
     * @return {Promise<any>}
   */
   const {convertFileToBase64} = useTools();

   /**
     * 防抖函数
     * @param method 事件触发的操作
     * @param delay 多次连续执行，只执行最后一次
     * @returns {Function}
   */

   const {debounce} = useTools();

   /**
     * 节流函数
     * @param method 事件触发的操作
     * @param mustRunDelay 连续多次触发，只执行第一次
   */
   const {throttle} = useTools();

   /**
     * 获取当前月的最后一天
     * @param {当前年} years 
     * @param {当前月} moth 
     * @returns 
   */
   const {getCurrentMothLastDay} = useTools();
    
   /**
     * 获取当前日期
     * @returns 
   */
   const {getToday} = useTools();
   /**
     * 获取当前计算机时间
     * @returns 
   */
   const {getComputerTime} = useTools();

   /**
     * 自动复制
     * @param {参数值} val 
   */
    const {autoCopy} = useTools();
 ```